/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controlapi.services;

import cl.controlapi.dao.ProductosJpaController;
import cl.controlapi.entity.Productos;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author GaCTuS
 */
@Path("productos")
public class ProductosRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarProductos() {
        ProductosJpaController daoProductos = new ProductosJpaController();
        List<Productos> listaProductos = daoProductos.findProductosEntities();
        return Response.ok(200).entity(listaProductos).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearProductos(Productos datosProductos) {
        ProductosJpaController daoProductos = new ProductosJpaController();
        Integer codProducto = datosProductos.getCodProducto();
        if (daoProductos.findProductos(codProducto) == null) { //Valido si el producto ingresado ya existe, de no ser así, lo inserto
            try {
                daoProductos.create(datosProductos);
                return Response.ok(200).entity(datosProductos).build();
            } catch (Exception ex) {
                return Response.ok(200).entity("{\"Error\":\"Ha ocurrido un error interno\"}").build();
            }
        } else {
            return Response.ok(200).entity("{\"Error\":\"El producto ingresado ya existe\"}").build();
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarProductos(Productos datosProductos) {
        ProductosJpaController daoProductos = new ProductosJpaController();
        Integer codProducto = datosProductos.getCodProducto();
        if (daoProductos.findProductos(codProducto) != null) {
            try {
                daoProductos.edit(datosProductos);
                return Response.ok(200).entity(datosProductos).build();
            } catch (Exception ex) {
                return Response.ok(200).entity("{\"Error\":\"Ha ocurrido un error interno\"}").build();
            }
        } else {
            return Response.ok(200).entity("{\"Error\":\"El producto consultado no existe\"}").build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{codProducto}")
    public Response getDatosProductos(@PathParam("codProducto") int codProducto) {
        ProductosJpaController daoProductos = new ProductosJpaController();
        try {
            Productos datosProductos = daoProductos.findProductos(codProducto);
            if (datosProductos != null) {
                return Response.ok(200).entity(datosProductos).build();
            } else {
                return Response.ok(200).entity("{\"Error\":\"El producto consultado no existe\"}").build();
            }
        } catch (Exception ex) {
            return Response.ok(200).entity("{\"Error\":\"Ha ocurrido un error interno\"}").build();
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{codProducto}")
    public Response deleteProductos(@PathParam("codProducto") int codProducto) {
        ProductosJpaController daoProductos = new ProductosJpaController();
        if (daoProductos.findProductos(codProducto) != null) {
            try {
                daoProductos.destroy(codProducto);
                return Response.ok(200).entity("{\"Atención\":\"Se ha eliminado el producto asociado al id " + codProducto + "\"}").build();
            } catch (Exception ex) {
                return Response.ok(200).entity("{\"Error\":\"Ha ocurrido un error interno\"}").build();
            }
        } else {
            return Response.ok(200).entity("{\"Error\":\"El producto consultado no existe\"}").build();
        }
    }
}
