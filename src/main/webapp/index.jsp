<%-- 
    Document   : index
    Created on : 17-jun-2021, 12:20:56
    Author     : GaCTuS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proyecto Evaluación Nº3</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
        <link rel="stylesheet" href="css/styles.min.css">
        <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
        <link href="css/font-awesome.min.css" rel="stylesheet" />
        <script type="text/javascript" src="js/validaciones.js"></script>
    </head>
    <body>
        <table width="600" cellspacing="1" cellpaddin="2" align="center">
            <thead>
            <th>
                <h2></h2>
            </th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <fieldset>
                        <legend>Evaluación nº3 (API's)</legend>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-action textosNormal"><span class="fa fa-bars"></span>&nbsp;LISTAR:&nbsp;&nbsp;https://evaluacion3apilgs.herokuapp.com/api/productos</li>
                            <li class="list-group-item list-group-item-action textosNormal"><span class="fa fa-eye"></span>&nbsp;CONSULTAR:&nbsp;&nbsp;https://evaluacion3apilgs.herokuapp.com/api/productos/5 <i>(valor ejemplo)</i></li>
                            <li class="list-group-item list-group-item-action textosNormal"><span class="fa fa-plus-square"></span>&nbsp;CREAR:&nbsp;&nbsp;https://evaluacion3apilgs.herokuapp.com/api/productos <i>(datos ejemplo)</i><br>
                                <fieldset style="width:300px;">
                                    {<br>
                                    "cantidadProductos": 50,<br>
                                    "codProducto": 8,<br>
                                    "marcaProducto": "Logitech",<br>
                                    "nombreProducto": "Luz Led Link Zelda",<br>
                                    "precioUnitario": 54990<br>
                                    }
                                </fieldset>
                            </li>
                            <li class="list-group-item list-group-item-action textosNormal"><span class="fa fa-edit"></span>&nbsp;EDITAR:&nbsp;&nbsp;https://evaluacion3apilgs.herokuapp.com/api/productos <i>(datos ejemplo)</i><br>
                                <fieldset style="width:300px;">
                                    {<br>
                                    "cantidadProductos": 70,<br>
                                    "codProducto": 8,<br>
                                    "marcaProducto": "Logitech",<br>
                                    "nombreProducto": "Luz Led Link Zelda",<br>
                                    "precioUnitario": 55990<br>
                                    }
                                </fieldset>
                            </li>
                            <li class="list-group-item list-group-item-action textosNormal"><span class="fa fa-eraser"></span>&nbsp;ELIMINAR:&nbsp;&nbsp;https://evaluacion3apilgs.herokuapp.com/api/productos/6 <i>(valor ejemplo)</i></li>
                        </ul>
                    </fieldset>
                </td>
            </tr>
        </tbody>
    </table>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
