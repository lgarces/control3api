//aquí redirijo al login.
function fnSalir() {
    window.location.href = "frmLogin.html";
}
//Aqui limpio los errores al intentar loguear
function fnLimpiarErrores() {
    document.getElementById('errores').innerHTML = ""
}
//Validación de rut (se puede modificar para estudiantes con pasaporte u otro tipo de documento)
function fnRut(rutCompleto)
{
    rutOk = rutCompleto.indexOf("-");
    arrRut = rutCompleto.split('-');
    if (rutOk != -1) {
        ruta = arrRut[0];
        dv = arrRut[1];
        var count = 0;
        var count2 = 0;
        var factor = 2;
        var suma = 0;
        var sum = 0;
        var digito = 0;
        count2 = ruta.length - 1;
        while (count < ruta.length)
        {
            sum = factor * (parseInt(ruta.substr(count2, 1)));
            suma = suma + sum;
            sum = 0;
            count = count + 1;
            count2 = count2 - 1;
            factor = factor + 1;

            if (factor > 7)
            {
                factor = 2;
            }

        }
        digito = 11 - (suma % 11)
        if (digito == 11)
        {
            digito = 0;
        }
        if (digito == 10)
        {
            digito = "K";
        }
        if (digito == dv.toUpperCase())
        {
            return 0;
        } else
        {
            return 1;
        }
    } else {
        return 1;
    }
}
//	Calculo la edad del estudiante
function calcularEdad(FechaNacimiento) {
    var fechaNace = new Date(FechaNacimiento);
    var fechaActual = new Date()
    var mes = fechaActual.getMonth();
    var dia = fechaActual.getDate();
    var ano = fechaActual.getFullYear();
    fechaActual.setDate(dia);
    fechaActual.setMonth(mes);
    fechaActual.setFullYear(ano);
    edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
    if (isNaN(edad)) {
        edad = '';
    }
    document.getElementById('txtEdad').value = edad;
}
//Válido el ingreso de un nuevo estudiante
function fnValidaIngresoEstudiante() {
    error = '';
    nombreEstudiante = document.getElementById('txtNombre').value;
    rutEstudiante = document.getElementById('txtRut').value;
    edadEstudiante = document.getElementById('txtEdad').value;
    direccionEstudiante = document.getElementById('txtDireccion').value;
    telefonoEstudiante = document.getElementById('txtTelefono').value;

    if (nombreEstudiante == '') {
        error = error + '<li>El campo Nombre estudiante es requerido</li>';
    }
    if (rutEstudiante == '') {
        error = error + '<li>El campo Rut estudiante es requerido</li>';
    } else {
        if (fnRut(rutEstudiante) == 1) {
            error = error + '<li>El rut ingresado no es válido</li>';
        }
    }
    if (edadEstudiante == '') {
        error = error + '<li>El campo Edad Estudiante es requerido</li>';
    } else {
        if (isNaN(edadEstudiante)) {
            error = error + '<li>El campo Edad Estudiante es incorrecto</li>';
        }
    }
    if (direccionEstudiante == '') {
        error = error + '<li>El campo Dirección Estudiante es requerido</li>';
    }
    if (telefonoEstudiante == '') {
        error = error + '<li>El campo Teléfono Estudiante es requerido</li>';
    } else {
        if (isNaN(telefonoEstudiante)) {
            error = error + '<li>El campo Teléfono Estudiante es incorrecto</li>';
        }
    }
    if (error != '') {
        document.getElementById('errores').innerHTML = '<fieldset class="anchoLegendUsuarios"><legend>Han ocurrido los siguientes errores:</legend>' + error + '</fieldset>';
    } else {
        document.frmIngresaEstudiantes.submit();
    }
}
function fnValidaEdicionEstudiante() {
    error = '';
    nombreEstudiante = document.getElementById('txtNombre').value;
    rutEstudiante = document.getElementById('txtRut').value;
    edadEstudiante = document.getElementById('txtEdad').value;
    direccionEstudiante = document.getElementById('txtDireccion').value;
    telefonoEstudiante = document.getElementById('txtTelefono').value;

    if (nombreEstudiante == '') {
        error = error + '<li>El campo Nombre estudiante es requerido</li>';
    }
    if (rutEstudiante == '') {
        error = error + '<li>El campo Rut estudiante es requerido</li>';
    } else {
        if (fnRut(rutEstudiante) == 1) {
            error = error + '<li>El rut ingresado no es válido</li>';
        }
    }
    if (edadEstudiante == '') {
        error = error + '<li>El campo Edad Estudiante es requerido</li>';
    } else {
        if (isNaN(edadEstudiante)) {
            error = error + '<li>El campo Edad Estudiante es incorrecto</li>';
        }
    }
    if (direccionEstudiante == '') {
        error = error + '<li>El campo Dirección Estudiante es requerido</li>';
    }
    if (telefonoEstudiante == '') {
        error = error + '<li>El campo Teléfono Estudiante es requerido</li>';
    } else {
        if (isNaN(telefonoEstudiante)) {
            error = error + '<li>El campo Teléfono Estudiante es incorrecto</li>';
        }
    }
    if (error != '') {
        document.getElementById('errores').innerHTML = '<fieldset class="anchoLegendEdicion"><legend>Han ocurrido los siguientes errores:</legend>' + error + '</fieldset>';
    } else {
        document.frmEstudiantes.submit();
    }
}
//Función para limpiar el formulario de ingreso de estudiantes
function fnLimpiaIngresoEstudiante() {
    document.getElementById('txtNombre').value = '';
    document.getElementById('txtRut').value = '';
    document.getElementById('txtEdad').value = '';
    document.getElementById('txtDireccion').value = '';
    document.getElementById('txtTelefono').value = '';
    fnLimpiarErrores();
}
/*  Válido la eliminación de un estudiante */
function fnEliminaEstudiante(rutEstudiante) {
    document.getElementById('hddRut').value = rutEstudiante;
    if (confirm('¿Está seguro de eliminar este registro?')) {
        document.frmEstudiantes.action = "EliminarController";
        document.frmEstudiantes.submit();
    } else {
        return false;
    }
}
function fnEditarEstudiante(rutEstudiante) {
    window.location.href = 'EditarController?rutEstudiante=' + rutEstudiante;
}
function fnVolverListado() {
    window.location.href = 'ListarController';
}
function fnVolverHome() {
    window.location.href = 'index.jsp';
}
function fnIngresaMantenedor() {
    window.location.href = 'ListarController';
}